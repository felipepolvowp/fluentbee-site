<?php get_header();?>

    <?php get_template_part( 'inc/title-internal' ); ?>

    <section class="box-top">
        <div class="container">
            <div class="grid-whoweare">
                <h2>Prazer, nós somos a <span>Fluent Bee.</span></h2>
                <figure>
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/banner-quem-somos.jpg" alt="Prazer, nós somos a Fluent Bee">
                    <a href="<?php echo site_url('/professores/');?>">Conheça nossos professores</a>
                </figure>
            </div>
        </div>
    </section>

    <section class="text-entry-about">
        <div class="text-box">
            <p>A Fluent Bee é uma escola de inglês construída por um time de professores apaixonados por ensinar inglês. Uma prova disso é que, somando a experiência de todos os integrantes de nossa equipe, reunimos décadas de experiência ensinando o idioma para brasileiros.</p>
            <p>E o que achamos muito interessante: todos nós sabemos falar português também! Mas não é só isso: amamos a cultura, as pessoas e o país como um todo — motivo pelo qual decidimos vir morar aqui no Brasil.</p>
        </div>
    </section>

    <section class="box-school">
        <figure>
            <img src="<?php echo get_template_directory_uri();?>/assets/img/img-box-about.png" alt="Uma escola de inglês séria">
        </figure>
        <div class="box-text">
            <h2>Uma escola de inglês séria</h2>
            <p>O maior diferencial da nossa escola de inglês é a certeza de que não vamos tentar vender “soluções mágicas” para você ficar fluente em poucas semanas, porque isso não existe. A nossa meta é te ajudar a alcançar a fluência em inglês usando técnicas comprovadas no mundo todo, com motivação para o estudo acontecer de maneira contínua.</p>
        </div>
    </section>

    <section class="text-end-about">
        <div class="text-box">
            <p>Nossos materiais escritos e vídeos são produzidos com base em livros e estudos de autores, universidades e editoras renomadas no mundo todo, como Jack Richards, Paul Nation, Cambridge University, School of Linguistics of Victoria University, Mc Graw Hill, entre outras.</p>
            <p>Também seguimos o Common European Framework of Reference for Languages (CEFR) — em português, Quadro Europeu Comum de Referência para Línguas, um padrão universal adotado para descrever as habilidades linguísticas.</p>
            <p>Em outras palavras, a Fluent Bee não é apenas uma escola de inglês. Ela é a união da paixão pelo Brasil e pelo Inglês, orientada para um aprendizado consistente e ganhos rápidos.</p>
        </div>
    </section>

    <section class="box-methods">
        <div class="grid-methods">
            <h2>Nosso método de ensino é para você que:</h2>
            <ul>
                <li>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/metodo-1.svg" alt="Método"></span>
                    <span class="text">Já <strong>CANSOU</strong> de estudar inglês e não consegue evoluir;</span>
                </li>
                <li>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/metodo-2.svg" alt="Método"></span>
                    <span class="text">Quer experimentar a <strong>LIBERDADE</strong> de poder conversar inglês com pessoas de todo o mundo;</span>
                </li>
                <li>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/metodo-3.svg" alt="Método"></span>
                    <span class="text">Tem o <strong>SONHO</strong> de ficar fluente em inglês para o resto da vida;</span>
                </li>
                <li>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/metodo-4.svg" alt="Método"></span>
                    <span class="text">E possui <strong>COMPROMETIMENTO</strong> para estudar inglês online de forma efetiva, com dedicação e sem truques mágicos.</span>
                </li>
            </ul>
        </div>
    </section>

    <?php get_template_part( 'inc/box-blog' ); ?>

<?php get_footer();?>