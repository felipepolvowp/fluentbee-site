    <footer class="footer">
        <div class="grid-footer">
            <div class="top">
                <nav class="menu-footer">
                    <ul>
                        <li><a href="<?php echo site_url('/');?>">Home</a></li>
                        <li><a href="<?php echo site_url('/quem-somos/');?>">Quem somos</a></li>
                        <li><a href="<?php echo site_url('/metodos-de-ensino/');?>">Método de ensino</a></li>
                        <li><a href="<?php echo site_url('/cursos/');?>">Cursos</a></li>
                    </ul>
                    <ul>
                        <li><a href="<?php echo site_url('/professores/');?>">Professores</a></li>
                        <li><a href="<?php echo site_url('/blog/');?>">Blog</a></li>
                        <li><a href="<?php echo site_url('/contato/');?>">Contato</a></li>
                    </ul>
                </nav>

                <div class="info">
                    <div class="logo">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/logo-black.svg" alt="Fluent Bee">
                        </a>
                    </div>
                    <div class="social-links">
                        <a href="https://www.facebook.com/fluentbee/" target="_blank"><span class="fa fa-facebook"></span></a>
                        <a href="https://www.youtube.com/channel/UCIEdtAMdoNreaQCO8a6XAjQ" target="_blank"><span class="fa fa-youtube-play"></span></a>
                        <a href="https://www.instagram.com/fluentbee/" target="_blank"><span class="fa fa-instagram"></span></a>
                        <a href="https://www.linkedin.com/company/11377056/"  target="_blank"><span class="fa fa-linkedin"></span></a>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <p>Fluent Bee - 2018 &copy; Todos os direitos reservados</p>
            </div>
        </div>
    </footer>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/bundle.min.js"></script>
</body>

</html>