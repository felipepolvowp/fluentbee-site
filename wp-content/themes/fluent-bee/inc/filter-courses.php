<aside class="course-filter">
    <h2>Filtros</h2>
    <div class="list">
        <h3>Nível</h3>
        <ul>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Iniciante</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Intermediário</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Avançado</label>
            </li>
        </ul>
    </div>

    <div class="list">
        <h3>Professor</h3>
        <ul>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Emilie Brunet</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Monique Felix</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Julian Montgomery</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Stephen Greene</label>
            </li>
        </ul>
    </div>

    <div class="list">
        <h3>Local</h3>
        <ul>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Nome do local</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Nome do local</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Nome do local</label>
            </li>
            <li>
                <div class="checkbox">
                    <span class="fa fa-check"></span>
                </div>
                <label>Nome do local</label>
            </li>
        </ul>
    </div>            
</aside>