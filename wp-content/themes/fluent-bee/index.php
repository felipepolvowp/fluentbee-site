<?php get_header(); ?>

    <section class="banner" data-vide-bg="<?php echo get_template_directory_uri();?>/assets/img/video.mp4" data-vide-options="loop: true, muted: true, position: 25% 25%">
        <div class="content">
            <div class="opacity">
                <div class="grid-banner">
                    <h1>English for <span>business.</span></h1>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="courses-next">
        <h2 class="ui-title">Próximos <br>cursos</h2>
        <div class="carousel">
            <div class="course">
                <span class="date">12 de Março, 2018</span>
                <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                <div class="teacher">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                    </div>
                    <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                </div>
                <a href="#" class="ui-button know-more">
                    <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                </a>
            </div>
            
            <div class="course">
                <span class="date">12 de Março, 2018</span>
                <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                <div class="teacher">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                    </div>
                    <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                </div>
                <a href="#" class="ui-button know-more">
                    <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                </a>
            </div>
            
            <div class="course">
                <span class="date">12 de Março, 2018</span>
                <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                <div class="teacher">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                    </div>
                    <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                </div>
                <a href="#" class="ui-button know-more">
                    <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                </a>
            </div>
            
            <div class="course">
                <span class="date">12 de Março, 2018</span>
                <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                <div class="teacher">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                    </div>
                    <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                </div>
                <a href="#" class="ui-button know-more">
                    <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                </a>
            </div>

            <div class="course">
                <span class="date">12 de Março, 2018</span>
                <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                <div class="teacher">
                    <div class="image">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                    </div>
                    <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                </div>
                <a href="#" class="ui-button know-more">
                    <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                    <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                </a>
            </div>
        </div>
        <div class="overlay"></div>
    </section>

    <section class="courses-about">
        <div class="box">
            <figure>
                <img src="<?php echo get_template_directory_uri();?>/assets/img/modulos-tematicos.png" alt="Módulos temáticos">
            </figure>
            <h3>Módulos temáticos</h3>
            <p>Aprenda inglês participado de aulas que tratem de assuntos específicos que sejam de seu interesse!</p>
        </div>
        <div class="box">
            <figure>
                <img src="<?php echo get_template_directory_uri();?>/assets/img/curta-duracao.png" alt="Curta duração">
            </figure>
            <h3>Curta duração</h3>
            <p>Cursos rápidos de poucas horas, ideais para quem não quer se comprometer com cursos de longa duração!</p>
        </div>
        <div class="box">
            <figure>
                <img src="<?php echo get_template_directory_uri();?>/assets/img/professores-nativos.png" alt="Professores nativos">
            </figure>
            <h3>Professores nativos</h3>
            <p>Nossa equipe é formada por professores da Austrália, Canadá, EUA e Inglaterra, e que moram no Brasil!</p>
        </div>
    </section>
    
    <section class="courses-test">
        <figure>
            <img src="<?php echo get_template_directory_uri();?>/assets/img/test.jpg" alt="Como está o seu inglês?">
        </figure>
        <div class="text">
            <span>Teste de nivelamento</span>
            <h2>Como está o seu inglês?</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate augue sem, at pretium. mauris interdum ac.</p>
            <a href="#">Faça o teste agora</a>
        </div>
    </section>

    <section class="courses-teachers">
        <div class="container">
            <h2 class="ui-title">Nossos <br>professores</h2>
            <div class="grid-teachers">
                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="blog-home">
        <div class="container">
            <div class="grid-blog-home">
                <div class="left">
                    <h2>Blog</h2>
                    <a href="#">Veja tudo</a>
                </div>
                <div class="right">
                    <a href="#" class="post">
                        <figure class="thumbnail">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/post.png" alt="Post Title">
                        </figure>
                        <div class="info">
                            <span class="category">Categoria</span>
                            <h3>Nome do notícia com até 3 linhas de texto lorem on ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate augue sem...</p>
                        </div>
                    </a>

                    <a href="#" class="post">
                        <figure class="thumbnail">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/post.png" alt="Post Title">
                        </figure>
                        <div class="info">
                            <span class="category">Categoria</span>
                            <h3>Nome do notícia com até 3 linhas de texto lorem on ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate augue sem...</p>
                        </div>
                    </a>

                    <a href="#" class="post">
                        <figure class="thumbnail">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/post.png" alt="Post Title">
                        </figure>
                        <div class="info">
                            <span class="category">Categoria</span>
                            <h3>Nome do notícia com até 3 linhas de texto lorem on ipsum dolor sit amet</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vulputate augue sem...</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>