       <?php get_header();?>

    <?php get_template_part( 'inc/title-internal' ); ?>

    <div class="container">
        <div class="grid-courses">
            <?php get_template_part( 'inc/filter-courses' ); ?>

            <main class="courses-main">
                <div class="order">
                    <span class="counter">Exibindo <strong>21</strong> de <strong>150</strong> cursos disponíveis</span>
                    <div class="select-order">
                        <span>Ordenar por:</span>
                        <select name="order">
                            <option value="most-recent">Mais recentes</option>
                        </select>
                    </div>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>

                <div class="course">
                    <span class="date">12 de Março, 2018</span>
                    <h3>Nome do curso com até 3 linhas de texto lorem ipsum dolor</h3>
                    <p>It is a long established fact that a reader will be distracted by the readable content of a page when...</p>
                    <div class="teacher">
                        <div class="image">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher.png" alt="Teacher">
                        </div>
                        <span class="teacher-name"><i>com</i><br>Emilie Brunet</span>
                    </div>
                    <a href="#" class="ui-button know-more">
                        <span class="text">Saiba mais&nbsp;&nbsp;&nbsp;</span>
                        <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/seta-branca.svg" alt="Saiba mais"></span>
                    </a>
                </div>
            </main>
        </div>
    </div>

<?php get_footer();?>