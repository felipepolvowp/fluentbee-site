$('.checkbox').click(function(){
    if(!$(this).hasClass('checked')){
        $(this).css({
            'background': '#0069f7',
            'border': 'none'
        })
        $(this).addClass('checked')
    } else  {
        $(this).css({
            'background': 'transparent',
            'border': '1px solid #e6e6e6'
        })
        $(this).removeClass('checked')
    }
})