$('.know-more').hover(function(){
    $(this).children('.text').delay(150).fadeIn(100)
    $(this).parent('.course').css({
        'border': '1px solid #0069f7',
        'boxShadow': '0 0 5px rgba(0,105,247,0.3)'
    })
}, function(){ 
    $(this).children('.text').hide()
    $(this).parent('.course').css({
        'border': '1px solid #e6e6e6',
        'boxShadow': 'none'
    })
})

$('.see-more').hover(function(){
    $(this).children('.text').delay(150).fadeIn(100)
    $(this).parent().find('figure').css({
        'border': '1px solid #0069f7'
    })
}, function(){ 
    $(this).children('.text').hide()
    $(this).parent().find('figure').css({
        'border': 'none'
    })
})