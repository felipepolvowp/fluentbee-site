/**
 * Components.
 */

import 'vide'

import './components/form'
import './components/carousel'
import './components/ui-button'
import './components/menu'
import './components/courses'