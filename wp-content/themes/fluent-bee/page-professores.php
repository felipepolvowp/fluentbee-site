<?php get_header();?>

    <?php get_template_part( 'inc/title-internal' ); ?>

    <section class="teachers-internal">
        <div class="container">
            <div class="grid-teachers">
                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>

                <div class="box">
                    <div class="image">
                        <figure>
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/teacher-2.png" alt="Teacher">
                        </figure>
                        <a href="#" class="ui-button see-more">
                            <span class="text">Veja</span>
                            <span class="icon"><img src="<?php echo get_template_directory_uri();?>/assets/img/plus.png" alt="Veja"></span>
                        </a>
                    </div>
                    <h3>Emilie Brunet</h3>
                    <p>Emilie Brunet é professora de inglês no Brasil desde 2010. É natural de Montreal, Canadá, e atualmente mora em Curitiba-PR.</p>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part( 'inc/box-blog' ); ?>

<?php get_footer(); ?>